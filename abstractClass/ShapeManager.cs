﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace abstractClass
{
    class ShapeManager
    {
        private List<Shape> shapes;

        public ShapeManager()
        {
            shapes=new List<Shape>();
        }

        public void AddShape(Shape shape)
        {
            shapes.Add(shape);
        }

        public double GetFullSqare()
        {
            double result = 0;

            foreach (var shape in shapes)
            {
                result += shape.GetSqare();
            }

            return result;
        }

    }
}
