﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace abstractClass
{
    class Circle:Shape
    {
        private double r;

        public Circle(double r)
        {
            this.r = r;
        }

        public override double GetSqare()
        {
            Console.WriteLine("circle");
            return Math.Pow(r, 2) * Math.PI;
        }
    }
}
